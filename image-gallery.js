// Allow us to use the Express framework
var express = require('express');

// TODO Ex 4 Step 1. Specify that the app should use fs (to scan directory contents)
var fs = require('fs');

// TODO Ex 5 Step 2. Specify that the app should use Formidable (to process file uploads) and Jimp (to create image thumbnails).
var formidable = require("formidable");
var jimp = require("jimp");

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Reads all the images in the public/images/thumbnails folder, then
// renders the image-gallery/image-gallery view.
function renderImageGallery(req, res) {

    // TODO Ex 4 Steps 2 through 4.
    fs.readdir('public/images/thumbnails/', function(err, files) {
        var data = {
            pageTitle: "Image Gallery",
            gallery: files,
        }

        res.render("image-gallery/image-gallery", data);
    });
   
}

// Specify that when we browse to "/" with a GET request, render the image gallery.
app.get('/', function (req, res) {
    renderImageGallery(req, res);
});

// TODO Ex 5 Steps 3 through 7. Process the file upload, generate thumbnail, display gallery.
app.post('/', function(req, res) {

    // 4. create a new formidable form object
    var form = new formidable.IncomingForm();

    // 5. when file upload detected, upload file to the fullsize images folder
    form.on("fileBegin", function (name, file) {
        file.path =__dirname + "/public/images/fullsize/" + file.name;
    });

    // 6/7. process fullsize image and generate thumbnail no greater than 400x400 px
    form.parse(req, function (err, fields, files) {
        var fullsizeUploadLocation = __dirname + '/public/images/fullsize/'
        var thumbLocation = __dirname + '/public/images/thumbnails/'
        var pathToFullsize = fullsizeUploadLocation + files.fileUpload.name;
        var pathToThumb = thumbLocation + files.fileUpload.name;

        jimp.read(pathToFullsize, function(err, image) {
            // .scaleToFit(width, height):
            image.scaleToFit(400, 400).write(pathToThumb, function() {
                renderImageGallery(req, res);
            });
        });
    });

});

// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});